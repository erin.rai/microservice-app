import { Subjects, Publisher, PaymentCreatedEvent } from '@erticket/common-pac';

export class PaymentCreatedPublisher extends Publisher<PaymentCreatedEvent> {
  subject: Subjects.PaymentCreated = Subjects.PaymentCreated;
}
