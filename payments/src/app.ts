import express from 'express';
import 'express-async-errors';
import { json } from 'body-parser';
import cookieSession from 'cookie-session';
import { createChargeRouter } from './routes/new';
import { errorHandler, NotFoundError, currentUser } from '@erticket/common-pac';

const app = express();
app.set('trust proxy', 1);
app.use(json());
app.use(
  cookieSession({
    signed: false,
    //add if secure true in prod "process.env.NODE_ENV !== 'test'"
    secure: false,
  })
);

app.use(currentUser);
app.use(createChargeRouter);

app.all('*', async (req, res) => {
  throw new NotFoundError();
});

app.use(errorHandler);

export { app };
