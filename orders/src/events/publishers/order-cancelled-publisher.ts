import { Publisher, OrderCancelledEvent, Subjects } from '@erticket/common-pac';

export class OrderCancelledPublisher extends Publisher<OrderCancelledEvent> {
  subject: Subjects.OrderCancelled = Subjects.OrderCancelled;
}
