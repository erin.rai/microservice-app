import { Publisher, OrderCreatedEvent, Subjects } from '@erticket/common-pac';

export class OrderCreatedPublisher extends Publisher<OrderCreatedEvent> {
  subject: Subjects.OrderCreated = Subjects.OrderCreated;
}
