import {
  Subjects,
  Publisher,
  ExpirationCompleteEvent,
} from '@erticket/common-pac';

export class ExpirationCompletePublisher extends Publisher<ExpirationCompleteEvent> {
  subject: Subjects.ExpirationComplete = Subjects.ExpirationComplete;
}
