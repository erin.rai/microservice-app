import { Publisher, Subjects, TicketUpdatedEvent } from '@erticket/common-pac';

export class TicketUpdatedPublisher extends Publisher<TicketUpdatedEvent> {
  subject: Subjects.TicketUpdated = Subjects.TicketUpdated;
}
