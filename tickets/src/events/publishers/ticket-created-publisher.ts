import { Publisher, Subjects, TicketCreatedEvent } from '@erticket/common-pac';

export class TicketCreatedPublisher extends Publisher<TicketCreatedEvent> {
  subject: Subjects.TicketCreated = Subjects.TicketCreated;
}
